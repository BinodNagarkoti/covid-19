import React, { Component } from "react";
import {Typography} from '@material-ui/core'
import styles from './Footer.module.css'
import nepalLogo from '../../images/nepal-logo.png'
import covid19 from '../../images/covid-19.png'
import { FaFacebook,FaTwitter,FaGithub,FaLinkedin } from "react-icons/fa";
import { IconContext } from "react-icons";
const Footer=()=>{
return (

    <div className={styles.container}>
        {/* <div className={styles.about}> */}
            {/* <Typography color="textSecondary" gutterBottom>
            Coronaviruses (CoV) are a large family of viruses that cause illness ranging from the common cold to more severe diseases such as Middle East Respiratory Syndrome (MERS-CoV) and Severe Acute Respiratory Syndrome (SARS-CoV). A novel coronavirus (nCoV) is a new strain that has not been previously identified in humans.
            </Typography> */}
        {/* </div> */}
        <div className={styles.app}>
        {/* <Typography color="textSecondary" gutterBottom> <img width='64' height='32'src={covid19}/> </Typography> */}
        <Typography> By <a href='https://github.com/BinodNagarkoti'>Binod Nagarkoti</a></Typography>
        <Typography className={styles.social_link}>
            <IconContext.Provider value={{ color: "#385898", size:'25px'}}>
            <a href="https://www.facebook.com/Binodb/"><FaFacebook/></a>
            </IconContext.Provider >
            <IconContext.Provider value={{ color: "rgb(29, 161, 242)", size:'25px'}}>
            <a href="https://twitter.com/binod1365"><FaTwitter/></a>
            </IconContext.Provider>
            <IconContext.Provider value={{ color: "#003f67", size:'25px'}}>
            <a href="https://www.linkedin.com/in/binod-nagarkoti-496245128?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3B0rrxJXsqT0SQGAfwBMDI7A%3D%3D"><FaLinkedin /></a>
            </IconContext.Provider>
            <IconContext.Provider value={{ color: "black", size:'25px'}}>
            <a href="https://github.com/BinodNagarkoti"><FaGithub /></a>
            </IconContext.Provider>
            </Typography>
            <Typography color='textSecondary' > May 31 2020 </Typography>
        </div>




    </div>
)

}
export default Footer